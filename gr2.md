```mermaid
flowchart LR
 h1[/i/]-.-> h2[режим:=<br>отдых];
 h2 -.-> h9((режим));
 h2 ==> par1([Время=Траб]);
par1 ==> h3[режим:=работа<br>Тод:=func__];
h3 ==> h4([Время=Тод]);
h4 ==> h5[Траб:=func__];
h10((Мастер)) -.->h6;
h5 ==> h6{Мастер<br>=...};
h6 ==>|"...= занят"| h7[Tрем:=Трем+<br>Траб-ВРЕМЯ];
h6 ==>|"...= свободен"| h2;
h7 -.-> h8((Трем));
h7 ==> h2;
h3 -.-> h9;
p11[/Траб=9ч/];

 h111([BPEMЯ=Tслом])==> h112[сост:=<br>сломан];
 h112 ==> h113([режим=<br>работа]);
 h113 ==> h114([мастер<br>=своб]);
 h114 ==> h115[мастер:=занят<br>Tрем:=funс/*/.];
 h115 ==> h116([ВРЕМЯ=Трем]);
 h116 ==> h117[сост:=рабочий<br>мастер:=своб<br>Tслом:=func/*/.];
 h117 ==> h111;
 h112 -.->par13((сост));
 h117 -.->par13((сост));
 h9-.->h113;
 h6-.->h114;
 h115 -.-> h10;
 h117 -.-> h10;
 h115 -.->h8;
 h8 -.-> h116;
 p99[/I/] -.- h111;
 h99[/Тслом = 100ч/];

 classDef cond fill:#bee,stroke:#aaa,stroke-width:1px;
 classDef state fill:#9e8,stroke:#333,stroke-width:1px;
 class h115,h118,h112 state;
 class h111,h113,h114,h117,h116 cond;

classDef cond fill:#bee,stroke:#aaa,stroke-width:1px;
classDef state fill:#9e8,stroke:#333,stroke-width:1px;
classDef navig fill:#eda,stroke:#333,stroke-width:1px;
classDef sdf fill:#ffc0cb ,stroke:#aaa,stroke-width:1px;
classDef sdau fill:#8b00ff,stroke:#aaa,stroke-width:1px;
class h2,h3,h5,h7,h117 state;
class par1,h4 cond;    
class h6 navig;
class h9,par13,h8 sdf;
class h10 sdau;
```
